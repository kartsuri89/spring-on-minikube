FROM eclipse-temurin:17-jdk-alpine
EXPOSE 8090
ADD target/spring-on-minikube-3.1.2.jar spring-on-minikube.jar
ENTRYPOINT ["java","-jar","/spring-on-minikube.jar"]