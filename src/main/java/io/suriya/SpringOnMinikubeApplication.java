package io.suriya;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringOnMinikubeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringOnMinikubeApplication.class, args);
	}

}
