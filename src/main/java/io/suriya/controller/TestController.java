package io.suriya.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	@GetMapping("/data")
	public String getResult() {
		return "Im from TestController - Running in pod";
	}
}
